﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.DAL.Models
{
    public class ItemModel
    {
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String ProductDescription { get; set; }
        public String ProductCategory { get; set; }
        public DateTime BiddingEndDate { get; set; }
    }
}
