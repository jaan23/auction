﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.DAL.Model
{
    public class Bid
    {
        public int BidId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public double Amount { get; set; }
        public string ItemId { get; set; }
        public DateTime Time { get; set; }
    }
}
