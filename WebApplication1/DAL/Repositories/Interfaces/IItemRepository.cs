﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL.Models;

namespace WebApplication1.DAL.Repositories.Interfaces
{
    public interface IItemRepository
    {
        List<ItemModel> GetItems();
    }
}
