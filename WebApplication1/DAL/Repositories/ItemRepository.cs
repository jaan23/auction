﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApplication1.DAL.Models;
using WebApplication1.DAL.Repositories.Interfaces;

namespace WebApplication1.DAL.Repositories
{
    public class ItemRepository : IItemRepository
    {

        private static string baseUrl = "http://uptime-auction-api.azurewebsites.net/api/";

        public List<ItemModel> GetItems()
        {
            var result = GetJsonResult(baseUrl + "Auction");
            List<ItemModel> items = JsonConvert.DeserializeObject<List<ItemModel>>(result);
            return items;
        }

        private static  String GetJsonResult(string url)
        {
            using (var httpClient = new HttpClient())
            {
                return  httpClient.GetStringAsync(url).Result;
            }
        }
    }
}
