﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL.Repositories.Interfaces;

namespace WebApplication1.DAL.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        private DbContext DbContext { get; set; }
        private DbSet<T> DbSet { get; set; }

        public BaseRepository(IAppDbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException(nameof(dbContext));
            DbContext = dbContext as DbContext;
            if (DbContext != null) DbSet = DbContext.Set<T>();
        }

        public void Add(T entity)
        {

            DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public List<T> All()
        {
            return DbSet.ToList();
        }

    }
}
