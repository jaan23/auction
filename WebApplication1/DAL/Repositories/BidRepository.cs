﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL.Model;
using WebApplication1.DAL.Repositories.Interfaces;

namespace WebApplication1.DAL.Repositories
{
    public class BidRepository : BaseRepository<Bid>, IBidRepository
    {
        public BidRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
