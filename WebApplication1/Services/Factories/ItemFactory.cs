﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL.Models;
using WebApplication1.Models;

namespace WebApplication1.Services.Factories
{
    public class ItemFactory
    {
        public ItemViewModel CreateItemViewModel(ItemModel item)
        {

            return new ItemViewModel()
            {
                BiddingEndDate = item.BiddingEndDate.ToString("R", new CultureInfo("de-DE")),
                ProductId = item.ProductId,
                ProductName = item.ProductName,
                ProductDescription = item.ProductDescription,
                ProductCategory = item.ProductCategory
              
    };
        }
    }
}
