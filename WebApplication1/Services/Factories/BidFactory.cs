﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL.Model;
using WebApplication1.Models;

namespace WebApplication1.Services.Factories
{
    public class BidFactory
    {
        public BidFormViewModel CreateItemViewModel(Bid bid)
        {
            return new BidFormViewModel()
            {
                Firstname = bid.Firstname,
                Lastname = bid.Lastname,
                Amount = bid.Amount,
            };
        }

        public Bid CreateBid(BidFormViewModel bid)
        {
            return new Bid()
            {
                Firstname = bid.Firstname,
                Lastname = bid.Lastname,
                ItemId = bid.Item.ProductId,
                Amount = bid.Amount,
            };
        }
    }
}
