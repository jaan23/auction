﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL;
using WebApplication1.DAL.Model;
using WebApplication1.DAL.Repositories;
using WebApplication1.DAL.Repositories.Interfaces;
using WebApplication1.Models;
using WebApplication1.Services.Factories;
using WebApplication1.Services.Interfaces;

namespace WebApplication1.Services
{
    public class BidService : IBidService
    {
        private IBidRepository bidRepository;

        public BidService(IBidRepository bidRepository)
        {
            this.bidRepository = bidRepository;
        }

        public void Add(BidFormViewModel bidForm)
        {
            DateTime time = DateTime.Now;
            DateTime biddingEndDate = DateTime.Parse(bidForm.Item.BiddingEndDate);
            if (time <= biddingEndDate)
            {
                Bid bid = new BidFactory().CreateBid(bidForm);
                bid.Time = time;
                bidRepository.Add(bid);
            }
       
        }

        public List<Bid> GetBid()
        {
            return bidRepository.All();
        }
    }
}
