﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL.Model;
using WebApplication1.Models;

namespace WebApplication1.Services.Interfaces
{
    public interface IBidService
    {
        void Add(BidFormViewModel bid);
        List<Bid> GetBid();
    }

}
