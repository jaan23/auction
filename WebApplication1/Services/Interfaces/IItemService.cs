﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Services.Interfaces
{
    public interface IItemService
    {
        List<ItemViewModel> GetItems();
        List<String> GetItemCategories();
        List<ItemViewModel> GetItemsByCategory(string category, List<ItemViewModel> items);
    }
}
