﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.DAL.Repositories;
using WebApplication1.Services.Interfaces;
using WebApplication1.DAL.Repositories.Interfaces;
using WebApplication1.Services.Factories;

namespace WebApplication1.Services
{
    public class ItemService : IItemService
    {
        IItemRepository itemRepository;

        public ItemService(IItemRepository itemRepository)
        {
            this.itemRepository = itemRepository;
        }

        public List<string> GetItemCategories()
        {
            return itemRepository.GetItems()
                .Select(item => item.ProductCategory)
                .Distinct()
                .ToList();
        }

        public List<ItemViewModel> GetItems()
        {
            return itemRepository.GetItems()
                .Select(item =>  new ItemFactory().CreateItemViewModel(item))
                .ToList();
        }

        public List<ItemViewModel> GetItemsByCategory(string category, List<ItemViewModel> items)
        {
            return items.Where(item => item.ProductCategory.ToLower().Equals(category.ToLower())).ToList();
        }
    }
}
