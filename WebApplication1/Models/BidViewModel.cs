﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class BidFormViewModel
    {
        public ItemViewModel Item {get;set;}

        [Required]
        [StringLength(100)]
        public string Firstname{ get; set; }

        [Required]
        [StringLength(100)]
        public string Lastname{ get; set; }

        [Required]
        [Range(0.0, Double.MaxValue)]
        public double Amount { get; set; }
    }
}
