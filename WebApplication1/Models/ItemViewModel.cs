﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ItemViewModel
    {
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String ProductDescription { get; set; }
        public String BiddingEndDate { get; set; }
        public String ProductCategory { get; set; }
    }
}
