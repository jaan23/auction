﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class AuctionViewModel
    {
        List<ItemViewModel> Items { get; set; }
        List<String> Categories { get; set; }
    }
}
