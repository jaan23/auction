﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.DAL.Model;
using WebApplication1.Models;
using WebApplication1.Services;
using WebApplication1.Services.Interfaces;

namespace WebApplication1.Controllers
{
    public class BidController : Controller
    {
        private IBidService bidService;

        public BidController(IBidService bidService)
        {
            this.bidService = bidService;
        }

        public IActionResult BidForm(ItemViewModel item)
        {
            var x = new BidFormViewModel { Item = item };
            return View(x);
        }

        public IActionResult AddBid(BidFormViewModel bid)
        {
            if (ModelState.IsValid)
            {
                bidService.Add(bid);
                return RedirectToAction("Index", "Item");
            }
            return View("BidForm", bid);
           
        }

        public IActionResult GetBid()
        {
            return View(bidService.GetBid());
        }
    }
}