﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Services;
using WebApplication1.Services.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    public class ItemController : Controller
    {
        IItemService itemService;

        public ItemController(IItemService itemService)
        {
            this.itemService = itemService;
        }


        public IActionResult Index(string category)
        {
            ViewBag.Categories = itemService.GetItemCategories();
            List<ItemViewModel> items = itemService.GetItems();

            if (category == null)
            {
                return View(items);
            }
            
            return View(itemService.GetItemsByCategory(category, items));
        }

        

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
